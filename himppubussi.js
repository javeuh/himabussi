let amount = 2; /* Number of busses */
const hslApiUrl = 'https://api.digitransit.fi/routing/v1/routers/hsl/index/graphql';
const alertTime = 6; /* Time remaining in minutes, then add Alert class red background etc */


$(".dropdown-menu button").click(function () {
  amount = Number($(this).val());
});

const stopInfoOptions = {
  method: 'POST',
  headers: {
    'Content-Type': 'application/json'
  },
  body: JSON.stringify({
    query: '{stop(id: "HSL:1531151") {name stoptimesWithoutPatterns {scheduledArrival realtimeArrival arrivalDelay scheduledDeparture realtimeDeparture departureDelay realtime realtimeState serviceDay headsign }}}'
  }),
};

const routesInfoOptions = {
  method: 'POST',
  headers: {
    'Content-Type': 'application/json'
  },
  body: JSON.stringify({
    query: '{ routes(name: "") { shortName longName patterns { code directionId name headsign }} }'
  }),
};

const grabContent = async (url, options) => await fetch(url, options)
  .then(res => {
    return res.json()
  })
  .catch(error => {
    console.log(error);
  });

function start(logData) {

  let combined = {
    'stopInfo': {},
    'routeInfo': {}
  };

  const data1 = grabContent(hslApiUrl, stopInfoOptions);
  const data2 = grabContent(hslApiUrl, routesInfoOptions);

  Promise.all([data1, data2])
    .then(datas => {
      combined['stopInfo'] = datas[0];
      combined['routeInfo'] = datas[1];
      logData(combined);
    })
}

/* Generate Arrival time in minutes and change css classes */

function timeToArrival(timeinseconds, serviceDay, index) {

  const arrivalTimeUtc = timeinseconds + serviceDay; /* Time must be set as UTC since 1970 */
  const timeNowInSeconds = Math.round(new Date() / 1000); /* Time must be set as UTC since 1970 */
  const difference = Math.floor((arrivalTimeUtc - timeNowInSeconds) / 60); /* Convert to minutes */

  if (arrivalTimeUtc >= timeNowInSeconds) {
    /* if-else only change for next bus comming - check index value */
    if (difference <= alertTime && index == 0) {
      animateAlert();
      return difference;

    } else if (difference > alertTime && index == 0) {
      removeAlert();
      return difference;

    } else {
      animateNormal();
      return difference;
    }

  } else {
    animateAlert();
    return '0';
  }
}

/* Get the line number from routes and stops data */

function convertToLineNumber(headsign, allRoutes) {
  for (let i = 0; i < allRoutes.length; i++) {
    const route = allRoutes[i];
    for (let k = 0; k < route.patterns.length; k++) {
      const pattern = route.patterns[k];
      if (pattern.headsign == headsign) {
        return route.shortName;
      } else {
        return headsign;
      }
    }
  }
}

/* start the app */
let startapp = setInterval(() => {
  function logData(data) {
    bussesConstructor(data.stopInfo, data.routeInfo);
  }
  start(logData); /* using callback to handle async */

}, 1000);

function restart() {
  clearInterval(startapp);
  startapp = setInterval(() => {
    function logData(data) {
      bussesConstructor(data.stopInfo, data.routeInfo);
    }
    start(logData); /* using callback to handle async */

  }, 1000);
}

/* Generate text content for the page*/

function bussesConstructor(stopData, routeData) {

  const stopRoutes = stopData.data.stop.stoptimesWithoutPatterns;
  const allRoutes = routeData.data.routes;
  let otherBusses = ["<div class='otherbusses'>"];

  for (let i = 0; i < stopRoutes.length; i++) {
    const element = stopRoutes[i];
    if (i == 0) {

      element.realtime ? document.getElementById('saapuu').textContent = timeToArrival(element.realtimeArrival, element.serviceDay, i) + ' min' : document.getElementById('saapuu').textContent = '~' + timeToArrival(element.scheduledArrival, element.serviceDay, i) + ' min';
      element.realtime ? document.getElementById('saapuutext').textContent = 'Live aika' : document.getElementById('saapuutext').textContent = 'Aikataulun mukaan';
      element.realtime ? $('body').addClass('liveSuccess') : $('body').removeClass('liveSuccess');
      element.realtime ? document.getElementById('linja').textContent = convertToLineNumber(element.headsign, allRoutes) : document.getElementById('linja').textContent = convertToLineNumber(element.headsign, allRoutes);
    } else if (i <= amount - 1) {

      const arrivalTime = "~" + timeToArrival(element.scheduledArrival, element.serviceDay, i) + ' min';
      const lineNumber = convertToLineNumber(element.headsign, allRoutes);
      otherBusses[i] = ("<div class='row mt-4 dimmed'> <div class='col-12 text-center dimmed'> <h6>Myöhemmin</h6> </div> <div class='col-12'> <p class='nextArrivaltime' id='saapuu2'>" + arrivalTime + "</p></div><div class='col-12 mb-1'><p class='busnumber2'><i class='fas fa-bus'>&nbsp;</i><span id='linja2'>" + lineNumber + "</span></p></div><hr class='kapea'></div>");
    }
  }
  otherBusses.push("</div>");
  $(".otherbusses").replaceWith(otherBusses.join(''));
}

function animateAlert() {
  $('.spinner').addClass('hide');
  $('.arrivaltime').removeClass('hide');
  $('body').removeClass('liveSuccess')
  $('body').addClass('alert');
  $('.arrivaltime').addClass('animatePulse');
}

function animateNormal() {
  $('.spinner').addClass('hide');
  $('.arrivaltime').removeClass('hide');
}

function removeAlert() {
  $('.spinner').addClass('hide');
  $('.arrivaltime').removeClass('hide');
  $('body').removeClass('alert');
  $('.arrivaltime').removeClass('animatePulse');
}